import { Injectable } from '@nestjs/common';
import { Command } from 'nestjs-command';

import driversData from '../../../seeds/drivers.json';
import { DriversService } from '../drivers.service';

@Injectable()
export class DriversSeed {
  constructor(
    private readonly driversService: DriversService
  ) {}

  @Command({ command: 'create:drivers' })
  async craete() {
    await this.driversService.createBulk(driversData);
  }
}
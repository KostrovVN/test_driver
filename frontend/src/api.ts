import queryString from 'query-string';

const host = process.env.API_URL || 'http://localhost:8080/api';

interface OptionsType {
  query?: Record<string, any>,
  method?: 'GET' | "PATCH",
  data?: any,
}

const baseFetch = async (path: string, options: OptionsType) => {
  let url = `${host}${path}`;
  const {
    query,
    data,
  } = options || {};
  const request: RequestInit = {
    method: options.method,
    headers: {
      "Content-Type": "application/json",
    },
  };

  if (query) {
    url += `?${queryString.stringify(query)}`;
  }

  if (data) {
    request.body = JSON.stringify(data);
  }
  console.log(request);
  

  return await fetch(url, request);
};  

const get = (path: string, options: OptionsType) => {
  return baseFetch(path, {
    ...options,
    method: 'GET',
  }) 
}
const path = (path: string, options: OptionsType) => {
  return baseFetch(path, {
    ...options,
    method: 'PATCH',
  });
};

export default {
  getDrivers(query: { limit: number, offset: number }) {
    return get('/drivers', { query });
  },

  updateDriver(driverid: string, data: any) {
    return path(`/drivers/${driverid}`, { data });
  }
}
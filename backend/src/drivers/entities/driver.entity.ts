import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { HydratedDocument } from 'mongoose';


@Schema()
export class Driver {
  @Prop()
  driverId: string;

  @Prop()
  url: string;

  @Prop()
  givenName: string;

  @Prop()
  familyName: string;

  @Prop()
  dateOfBirth: string;

  @Prop()
  nationality: string;
}

export type DriverDocument = HydratedDocument<Driver>;

export const DriverSchema = SchemaFactory.createForClass(Driver);
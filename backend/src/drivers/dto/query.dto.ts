import Joi from "joi";
import { JoiSchema } from "nestjs-joi";

export class QueryDto {
  @JoiSchema(Joi.number().min(20).max(30))
  limit: number;
 
  @JoiSchema(Joi.number())
  offset: number;
}
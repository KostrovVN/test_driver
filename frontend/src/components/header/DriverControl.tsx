import React, { memo } from 'react';
import { useSelector } from 'react-redux';
import { Button, StyleSheet, View } from 'react-native';
import { selectDriver } from '../../features/driverSlice';
import { useNavigation } from '@react-navigation/native';



const DriverControl = () => {
  const { data } = useSelector(selectDriver);

  const navigation = useNavigation()

  return (
    <View style={styles.button}>
      <Button
        title="edit"
        onPress={() => navigation.navigate("Driver edit", data)}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  button: {
    marginHorizontal: 20,
  },
})

export default memo(DriverControl);

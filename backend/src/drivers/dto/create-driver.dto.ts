import { JoiSchema, JoiSchemaOptions } from "nestjs-joi";
import { driverValidation } from 'common';

@JoiSchemaOptions({
  allowUnknown: false,
})
export class CreateDriverDto {
  @JoiSchema(driverValidation.driverId)
  driverId: string;

  @JoiSchema(driverValidation.url)
  url: string;

  @JoiSchema(driverValidation.givenName)
  givenName: string;

  @JoiSchema(driverValidation.familyName)
  familyName: string;

  @JoiSchema(driverValidation.dateOfBirth)
  dateOfBirth: string;

  @JoiSchema(driverValidation.nationality)
  nationality: string;
}

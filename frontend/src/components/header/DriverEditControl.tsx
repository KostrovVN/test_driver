import React, { memo } from 'react';
import { selectDriverEditHasErrors, selectDriverEditIsChanged, useUpdateDriver } from '../../features/driverEditSlice';
import { useSelector } from 'react-redux';
import { Button, StyleSheet, View } from 'react-native';



const DriverEditControl = () => {
  const isChanged = useSelector(selectDriverEditIsChanged);
  const hasError = useSelector(selectDriverEditHasErrors);

  const update = useUpdateDriver()

  return (
    <View style={styles.button}>
      <Button
        title="save"
        disabled={!isChanged || hasError}
        onPress={() => update()}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  button: {
    marginHorizontal: 20,
  },
})

export default memo(DriverEditControl);

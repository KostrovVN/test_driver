import { combineReducers, configureStore } from '@reduxjs/toolkit'
import drivers from './features/driversSlice';
import editDriver from './features/driverEditSlice';
import driver from './features/driverSlice';
import { persistReducer } from 'redux-persist'
import storage from 'redux-persist/lib/storage';
import thunk from 'redux-thunk'


const persistConfig = {
  key: 'root',
  storage,
  whitelist: ['driver']
}

const reducers = combineReducers({
  drivers,
  driver,
  editDriver,
});

const persistedReducer = persistReducer(persistConfig, reducers);


export const store = configureStore({
  reducer: persistedReducer,
  devTools: process.env.NODE_ENV !== 'production',
  middleware: [thunk]

});


export type RootState = ReturnType<typeof store.getState>
export type AppDispatch = typeof store.dispatch
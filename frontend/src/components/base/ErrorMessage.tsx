import React, { memo } from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';

type ErrorMessagePropsType = {
  message: string | null,
  retry: () => void,
};

const ErrorMessage = (props: ErrorMessagePropsType) => {
  const {
    message,
    retry,
  } = props

  return (
    <View style={styles.container}>
      <Text style={styles.message}>
        {message || 'unknown error'}
      </Text>
      {retry && (
        <TouchableOpacity onPress={retry}>
          <Text style={styles.retry}>
            retry
          </Text>
        </TouchableOpacity>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  message: {
    color: 'red',
    marginBottom: 20,
  },
  retry: {
    color: 'rgb(33, 150, 243)',
    fontSize: 18,
  }
})

export default memo(ErrorMessage);
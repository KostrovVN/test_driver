import { RouteProp, useNavigation, useRoute } from '@react-navigation/native';
import React, { useEffect } from 'react';
import { DriverType } from 'common';
import { useDispatch, useSelector } from 'react-redux';
import { AppDispatch } from '../store';
import { driverEdit, selectDriverEditStatus } from '../features/driverEditSlice';
import DriverEditForm from '../components/DriverEditForm/DriverEditForm';

type DriverEditScreenRoute = RouteProp<{
    params: DriverType,
}>
const DriverEditScreen = () => {
  const { params } = useRoute<DriverEditScreenRoute>();
  const dispatch = useDispatch<AppDispatch>();
  const status = useSelector(selectDriverEditStatus);
  const navigation = useNavigation();
  
  
  useEffect(
    () => {
      dispatch(driverEdit.actions.start(params))
      return () => {
        dispatch(driverEdit.actions.clear())
      };
    },
    [],
  );

  useEffect(
    () => {
      if (status === 'succeeded') {
        navigation.goBack();
      }
    },
    [status],
  )

  return (
    <DriverEditForm />
  );
};

export default DriverEditScreen;
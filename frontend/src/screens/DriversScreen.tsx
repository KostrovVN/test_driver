import React, { memo, useCallback, useEffect } from "react"
import { ActivityIndicator, FlatList, StyleProp, Text, View, ViewStyle } from "react-native";
import { fetchDrivers, selectDrivers } from "../features/driversSlice";
import { useDispatch, useSelector } from "react-redux";
import { AppDispatch } from "../store";
import { SafeAreaView } from "react-native-safe-area-context";
import DriverItem from "../components/DriverItem/DriverItem";
import ErrorMessage from "../components/base/ErrorMessage";
import ScreenLoading from "../components/base/ScreenLoading";

const DriversScreen = () => {
  const dispatch = useDispatch<AppDispatch>();
  const {
    limit,
    offset,
    status,
    data,
    errors,
    finished,
  } = useSelector(selectDrivers)

  const next = useCallback(
    () => {
      dispatch(fetchDrivers({ limit, offset }))
    },
    [limit, offset],
  )

  useEffect(
    () => {
      next();
    },
    [],
  );

  const isLoading = status === 'loading';

  if (isLoading && data.length === 0 ) {
    return (
      <ScreenLoading />
    );
  }

  if (status === 'failed') {
    return (
      <ErrorMessage
        message={errors}
        retry={() => next()}
      />
    );
  }

  if (finished && data.length === 0) {
    return (
      <View style={{ flex: 1,  justifyContent: 'center'}}>
        <Text style={{ textAlign: 'center' }}>
          Empty list
        </Text>
      </View>
    );
  }

  return (
    <SafeAreaView style={styles.container}>
      <FlatList
        data={data}
        // initialNumToRender={limit}
        renderItem={({ item }) => (
          <DriverItem item={item} />
        )}
        keyExtractor={item => {
          return item.driverId
        }}
        ListEmptyComponent={
          <ActivityIndicator />
        }
        refreshing={isLoading}
        onEndReached={() => {
          if(!isLoading && !finished) {
            next();
          }
        }}
      />
    </SafeAreaView>
  )
}


const styles: Record<string, StyleProp<ViewStyle>> = {
  container: {
    flex: 1,
  }
}

export default memo(DriversScreen);
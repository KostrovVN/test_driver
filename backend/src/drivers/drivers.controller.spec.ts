import { Test, TestingModule } from '@nestjs/testing';
import { DriversController } from './drivers.controller';
import { DriversService } from './drivers.service';
import { getModelToken } from '@nestjs/mongoose';
import { Driver } from './entities/driver.entity';
import { Model } from 'mongoose';

describe('DriversController', () => {
  let controller: DriversController;
  let driversService: DriversService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [DriversController],
      providers: [
        {
          provide: getModelToken(Driver.name),
          useValue: Model,
        },
        DriversService
      ],
    }).compile();

    driversService = module.get<DriversService>(DriversService)
    controller = module.get<DriversController>(DriversController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  describe('findAll', () => {
    it('should return array of drivers', async () => {
      const result: any = [];
      jest.spyOn(driversService, 'findAll').mockImplementation(() => result);

      expect(await controller.findAll({ limit: 1, offset: 10 }));
    });
  });

  describe('update', () => {
    it('should validate', async () => {
      jest.spyOn(driversService, 'update').mockImplementation(() => null);

      expect(await controller.update('test', { url: 'hello' }));
    });
  });
});

import { Module } from '@nestjs/common';
import { DriversService } from './drivers.service';
import { DriversController } from './drivers.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { Driver, DriverSchema } from './entities/driver.entity';
import { DriversSeed } from './seeds/drivers.seeds';
import { JoiPipeModule } from 'nestjs-joi';

@Module({
  imports: [
    MongooseModule.forFeature([ { name: Driver.name, schema: DriverSchema }]),
    JoiPipeModule,
  ],
  controllers: [DriversController],
  providers: [DriversService, DriversSeed],
  exports: [DriversSeed],
})
export class DriversModule {}

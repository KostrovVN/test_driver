import { driverValidation } from 'common';
import { JoiSchema } from 'nestjs-joi';

// @JoiSchemaOptions({ allowUnknown: true })
export class UpdateDriverDto {
  @JoiSchema(driverValidation.url)
  url?: string;

  @JoiSchema(driverValidation.givenName)
  givenName?: string;

  @JoiSchema(driverValidation.familyName)
  familyName?: string;

  @JoiSchema(driverValidation.dateOfBirth)
  dateOfBirth?: string;

  @JoiSchema(driverValidation.nationality)
  nationality?: string;
}

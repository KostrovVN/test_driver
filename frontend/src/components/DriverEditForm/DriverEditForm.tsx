import React, { memo, useCallback } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { ChangedDriverPayload, DriverChingingType, ValidateDriverPayload, driverEdit, selectDriverEdit } from '../../features/driverEditSlice';
import { Text, View } from 'react-native';
import TextField from '../base/TextField/TextField';
import { AppDispatch } from '../../store';

const DriverEditForm = () => {
  const {
    data,
    changed,
    itemErrors,
    errors,
  } = useSelector(selectDriverEdit);

  const dispatch = useDispatch<AppDispatch>();

  const change = useCallback(
    ({ name, value }: ChangedDriverPayload) => {
      dispatch(driverEdit.actions.change({ name, value }))
    },
    [],
  );
  const validate = useCallback(
    ({ name }: ValidateDriverPayload) => {
      dispatch(driverEdit.actions.validate({ name }))
    },
    [],
  );

  const inputNames: (keyof DriverChingingType)[] = [
    'url',
    'givenName',
    'familyName',
    'dateOfBirth',
    'nationality',
  ];

  if (!data) {
    return;
  }

  return (
    <View>
      {!!errors && (
        <View>
          <Text style={{ color: 'red', marginVertical: 20, textAlign: 'center'}}>
            {errors}
          </Text>
        </View>
      )}
      {inputNames.map((name) => (
        <TextField
          key={name}
          name={name}
          value={changed[name] !== undefined ? changed[name] : data[name]}
          error={itemErrors[name]}
          onChange={change}
          onBlur={validate}
        />
      ))}
    </View>
  );
};

export default memo(DriverEditForm);
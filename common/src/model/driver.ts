import Joi from 'joi';

export interface DriverType {
  driverId: string;
  url: string;
  givenName: string;
  familyName: string;
  dateOfBirth: string;
  nationality: string;
}

export const driverValidation = {
  driverId: Joi.string(),
  url: Joi.string().uri(),
  givenName: Joi.string(),
  familyName: Joi.string(),
  dateOfBirth: Joi.date(),
  nationality: Joi.string(),
}

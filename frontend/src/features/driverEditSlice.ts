import { createAsyncThunk, createSelector, createSlice, type PayloadAction } from "@reduxjs/toolkit"

import { DriverType, driverValidation } from 'common';
import { AppDispatch, RootState } from "../store";
import api from '../api';
import { useDispatch, useSelector } from 'react-redux';
import { useCallback } from 'react';

export type DriverChingingType = Omit<DriverType, 'driverId'>
type ChangedType = Partial<DriverType>;

export type ChangedDriverPayload = {
  name: keyof DriverChingingType,
  value: string,
};

export type ValidateDriverPayload = {
  name: keyof DriverChingingType,
};


interface EditDriverState {
  data: null | DriverType,
  changed: ChangedType,
  itemErrors: Partial<{ [Property in keyof DriverChingingType]: string }>;
  errors: null | string,
  status: 'idle' | 'saving' | 'succeeded' | 'failed'
};

const initialState = {
  data: null,
  changed: {},
  itemErrors: {},
  errors: null,
  status: 'idle'
} as EditDriverState;

export const updateDriver = createAsyncThunk(
  'drivers/update',
  async (driverData: { driverId: string, changed: EditDriverState['changed'] }, { rejectWithValue }) => {
    const {
      driverId,
      changed,
    } = driverData;
    const response = await api.updateDriver(driverId, changed)
    const data = await response.json();

    if (response.status !== 200) {
      return rejectWithValue(data.message);
    }
    return data;
  })


export const driverEdit = createSlice({
  name: 'editDriver',
  initialState,
  reducers: {
    start(state, action: PayloadAction<DriverType>) {
      state.data = action.payload;
    },
    validate(state, { payload: { name } }: PayloadAction<ValidateDriverPayload> ) {
      const { error } = driverValidation[name].validate(state.changed[name]);
      if (error) {
        state.itemErrors[name] = error.message.replace('value', name);
      }
    },
    change(state, { payload: { name, value } }: PayloadAction<ChangedDriverPayload>) {
      state.changed[name] = value;
      state.errors = null;
      state.status = 'idle';
    },
    clear() {
      return initialState;
    }
  },
  extraReducers: (builder) => {
    builder.addCase(updateDriver.fulfilled, (state, action) => {
      state.status = 'succeeded';
    });
    builder.addCase(updateDriver.pending, (state) => {
      state.status = 'saving';
    })
    builder.addCase(updateDriver.rejected, (state, action) => {
      state.status = 'failed';
      state.errors = String(action.payload) || 'unknown error'
    });
  },
})


export const selectDriverEdit = (state: RootState) => state.editDriver;


const selectChanged = createSelector(
  selectDriverEdit,
  (editDriver) => editDriver.changed,
);

const selectData = createSelector(
  selectDriverEdit,
  (editDriver) => editDriver.data,
);

const seelectItemErrors = createSelector(
  selectDriverEdit,
  (editDriver) => editDriver.itemErrors,
);

export const selectDriverEditStatus = createSelector(
  selectDriverEdit,
  (editDriver) => editDriver.status,
)

export const selectDriverEditIsChanged = createSelector(
  selectChanged,
  selectData,
  (changed, data) => {
    if (!data) {
      return false;
    }

    for (const key in changed) {
      if (data[key as keyof ChangedType] !== changed[key as keyof ChangedType] ) {
        return true;
      }
    }
    return false;
  },
);
export const selectDriverEditHasErrors = createSelector(
  seelectItemErrors,
  (itemErrors) => {
    return Object.keys(itemErrors).length > 0;
  },
)

export const useUpdateDriver = () => {
  const data = useSelector(selectData);
  const changed = useSelector(selectChanged);
  const dispatch = useDispatch<AppDispatch>();

  return useCallback(
    () => {
      if (data) {
        dispatch(updateDriver({ driverId: data.driverId, changed }));
      }
    },
    [data, changed],
  )
}



export default driverEdit.reducer;
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { Provider } from 'react-redux'
import { store } from './store';
import DriversScreen from './screens/DriversScreen';
import DriverEditScreen from './screens/DriverEditScreen';
import DriverEditControl from './components/header/DriverEditControl';
import DriverScreen from './screens/DriverScreen';
import DriverControl from './components/header/DriverControl';
import { persistStore } from 'redux-persist';
import { PersistGate } from 'redux-persist/integration/react'


export default function App() {
  const Stack = createNativeStackNavigator();
  let persistor = persistStore(store);

  return (
      <Provider store={store}>
        <PersistGate loading={null} persistor={persistor}>
          <NavigationContainer>
            <Stack.Navigator initialRouteName='Drivers'>
              <Stack.Screen
                name="Drivers"
                component={DriversScreen}
              />
              <Stack.Screen
                name="Driver"
                component={DriverScreen}
                options={{
                  headerRight: () => <DriverControl />
                }}
              />
              <Stack.Screen
                name="Driver edit"
                component={DriverEditScreen}
                options={{
                  headerRight: () => <DriverEditControl />
                }}
              />
            </Stack.Navigator>
          </NavigationContainer>
        </PersistGate>
      </Provider>
  );
};
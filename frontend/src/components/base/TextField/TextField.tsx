import React, { memo } from "react";
import { StyleSheet, Text, TextInput, View } from "react-native";

interface TextFieldPropsType<T extends string = any> {
  value?: string;
  name: T;
  onChange: (e: { name: T, value: string }) => void;
  onBlur: (e: { name: T }) => void,
  error?: null | string,
};

const TextField = (props: TextFieldPropsType) => {
  const {
    name,
    value,
    error,
    onChange,
    onBlur,
  } = props;

  const hasError = !!error;

  return (
    <View style={styles.wrapper}>
      <Text style={styles.title}>
        {name}
      </Text>
      <TextInput
        style={[styles.input, !!hasError && styles.error]}
        value={value || ''}
        onChangeText={(value) => onChange({ name, value })}
        onBlur={() => onBlur({ name })}
      />
      {!!error && (
        <Text style={styles.errorMessage}>
          {error}
        </Text>
      )}
    </View>
  );  
};

const styles = StyleSheet.create({
  error: {
    borderColor: 'red',
    color: 'red',
  },
  errorMessage: {
    marginTop: 12,
    color: 'red',
  },
  wrapper: {
    padding: 20,
  },
  title: {
    fontSize: 18,
    textTransform: 'capitalize',
    marginBottom: 12,
  },
  input: {
    padding: 10,
    borderWidth: 1,
    borderRadius: 4,
    fontSize: 18,
  },
})

export default memo(TextField)
import React, { memo, useEffect, useState } from "react"
import { StyleSheet, Text, View } from "react-native";
import { useDispatch, useSelector } from "react-redux";
import { AppDispatch } from "../store";
import ScreenLoading from "../components/base/ScreenLoading";
import { DriverType } from "common";
import { RouteProp, useNavigation, useRoute } from "@react-navigation/native";
import { driver, selectDriver } from "../features/driverSlice";

type DriverEditScreenRoute = RouteProp<{
  params: DriverType,
}>

const DriverScreen = () => {
  const { params } = useRoute<DriverEditScreenRoute>();
  const { data } = useSelector(selectDriver);
  const dispatch = useDispatch<AppDispatch>();


  useEffect(
    () => {
      dispatch(driver.actions.load(params));
    },
    [],
  );
  

  if (!data) {
    return (
      <ScreenLoading />
    )
  }

  const {
    driverId,
    url,
    givenName,
    familyName,
    dateOfBirth,
    nationality,
  } = data;

  return (
    <View style={styles.container}>
      <Text style={styles.title}>
        {givenName} {familyName}
      </Text>
      <Text>
        {dateOfBirth}
      </Text>
      <Text>
        {nationality}
      </Text>
      <Text style={styles.url}>
        {url}
      </Text>
    </View>
  )
}

const styles = StyleSheet.create({
  title: {
    fontSize: 18,
    marginBottom: 12,
  },
  container: {
    padding: 20,
    borderBottomWidth: 1,
  },
  button: {
    maxWidth: 200,
  },
  url: {
    marginBottom: 10,
  }
});



export default memo(DriverScreen);
import { Injectable } from '@nestjs/common';
import { CreateDriverDto } from './dto/create-driver.dto';
import { UpdateDriverDto } from './dto/update-driver.dto';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { Driver } from './entities/driver.entity';
import { QueryDto } from './dto/query.dto';

@Injectable()
export class DriversService {
  constructor(
    @InjectModel(Driver.name) private driverModel: Model<Driver>
  ){}

  create(createDriverDto: CreateDriverDto): Promise<Driver> {
    const driver = new this.driverModel(createDriverDto);
    return driver.save();
  }

  createBulk(createDriverDto: CreateDriverDto[]): Promise<Driver[]> {
    return this.driverModel.insertMany(createDriverDto)
  }

  findAll(query: QueryDto) {
    return this.driverModel.find().skip(query.offset).limit(query.limit);
  }

  findOne(id: number) {
    return `This action returns a #${id} driver`;
  }

  update(id: string, updateDriverDto: UpdateDriverDto) {
    return this.driverModel.findOneAndUpdate(
      { driverId: id },
      updateDriverDto,
      { new: true },
    )
  }
}

import { useNavigation } from '@react-navigation/native';
import { DriverType } from 'common';
import React, { memo } from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';

export interface DriverItemType {
  item: DriverType,
}

const styles = StyleSheet.create({
  title: {
    fontSize: 18,
    marginBottom: 12,
  },
  container: {
    padding: 20,
    borderBottomWidth: 1,
  },
  details: {
    flexDirection: 'row',
    flex: 1,
    marginBottom: 12,
    justifyContent: 'space-between',
  },
  button: {
    maxWidth: 200,
  },
  url: {
    marginBottom: 10,
  }
});

const DriverItem = ({ item }: DriverItemType) => {
  const {
    driverId,
    url,
    givenName,
    familyName,
    dateOfBirth,
    nationality,
  } = item;
  const navigation = useNavigation();

  return (
    <View style={styles.container}>
      <TouchableOpacity
        onPress={() => navigation.navigate("Driver", item)}
      >
        <Text style={styles.title}>
          {givenName} {familyName}
        </Text>
      </TouchableOpacity>

      <View style={styles.details}>
        <Text>
          {dateOfBirth}
        </Text>
        <Text>
          {nationality}
        </Text>
      </View>

      <Text style={styles.url}>
        {url}
      </Text>
    </View>
  );
}

export default memo(DriverItem)
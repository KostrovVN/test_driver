import { PayloadAction, createSlice } from "@reduxjs/toolkit";
import { DriverType } from 'common';
;
import { RootState } from "../store";
import { updateDriver } from "./driverEditSlice";

interface DriversState {
  data: null | DriverType,
}

const initialState = {
  data: null,
} as DriversState;

export const driver = createSlice({
  name: 'drivers',
  initialState,
  reducers: {
    load(state, action) {
      state.data = action.payload;
    },
  },
  extraReducers: (builder) => {
    builder.addCase(updateDriver.fulfilled, (state, action: PayloadAction<DriverType>) => {
      state.data = action.payload;
    });
  }
})
export const selectDriver = (state: RootState) => state.driver;

export default driver.reducer;
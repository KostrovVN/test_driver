import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { DriversModule } from './drivers/drivers.module';
import { MongooseModule } from '@nestjs/mongoose';
import { CommandModule } from 'nestjs-command';

const {
  CONFIG_MONGODB_HOST,
  CONFIG_MONGODB_PASSWORD,
  CONFIG_MONGODB_USER,
  CONFIG_MONGODB_PORT,
} = process.env;

@Module({
  imports: [
    CommandModule,
    MongooseModule.forRoot(`mongodb://${CONFIG_MONGODB_HOST}:${CONFIG_MONGODB_PORT}`, {
      dbName: 'driverdb',
      pass: CONFIG_MONGODB_PASSWORD,
      user: CONFIG_MONGODB_USER,
    }),
    DriversModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}

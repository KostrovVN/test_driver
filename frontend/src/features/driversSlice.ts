import { PayloadAction, createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import { DriverType } from 'common';

import api from "../api";
import { RootState } from "../store";
import { updateDriver } from "./driverEditSlice";

interface DriversState {
  data: DriverType[]
  offset: number,
  limit: 20,
  status: 'idle' | 'loading' | 'succeeded' | 'failed'
  errors: string| null,
  finished: boolean,
}

const initialState = {
  status: 'loading',
  data: [],
  offset: 0,
  limit: 20,
  errors: null,
  finished: false,
} as DriversState;

export const fetchDrivers = createAsyncThunk(
  'drivers/fetchDrivers',
  async (query: Parameters<typeof api.getDrivers>[0], { rejectWithValue }) => {
    const response = await api.getDrivers(query);
          
    const data = await response.json();

    if (response.status !== 200) {
      return rejectWithValue(response.statusText);
    }

    return data
  },
)

export const selectDrivers = (state: RootState) => state.drivers;


export const drivers = createSlice({
  name: 'drivers',
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(updateDriver.fulfilled, (state, action: PayloadAction<DriverType>) => {
      const index = state.data.findIndex((item) => {
        return item.driverId === action.payload.driverId;
      })
      state.data[index] = action.payload;
    });
    builder.addCase(fetchDrivers.fulfilled, (state, action) => {
      state.status = 'succeeded';
      state.data = state.data.concat(action.payload);
      state.offset += action.meta.arg.limit;
      if (state.data.length === 0){
        state.finished = true;
      }
    });
    builder.addCase(fetchDrivers.pending, (state) => {
      state.status = 'loading';
    });
    builder.addCase(fetchDrivers.rejected, (state, action) => {
      state.status = 'failed';
      state.errors = String(action.payload) || 'unknown error';
    })
  },
})


export default drivers.reducer;
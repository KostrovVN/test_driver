import React, { memo } from 'react';
import { ActivityIndicator, StyleSheet, View } from 'react-native';

const ScreenLoading = () => {
  return (
    <View style={styles.container}>
      <ActivityIndicator />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  }
})

export default memo(ScreenLoading);